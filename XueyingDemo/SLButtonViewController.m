//
//  ViewController.m
//  SLButtonSample
//
//  Created by Ali Pourhadi on 2015-09-29.
//  Copyright © 2015 Ali Pourhadi. All rights reserved.
//

#import "SLButtonViewController.h"
#import "SLButton.h"

@interface SLButtonViewController ()
@end
@implementation SLButtonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	SLButton *centerButton = [[SLButton alloc]initWithFrame:CGRectMake(0, 0, 200, 60)];
	[centerButton setTitle:@"Center Button" forState:UIControlStateNormal];
    centerButton.layer.masksToBounds = YES;
    centerButton.layer.cornerRadius = 4;
	[centerButton setBackgroundColor:[UIColor orangeColor]];
	[centerButton setCenter:self.view.center];
	
	[centerButton setBackgroundBlock:^{
		NSLog(@"In the Background Thread");
	} MainThreadBlock:^{
		NSLog(@"In the Main Thread");
	} forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:centerButton];
}

@end
