//
//  ViewController.m
//  ChinaCityList
//
//  Created by zjq on 15/10/27.
//  Copyright © 2015年 zhengjq. All rights reserved.
//

#import "ChinaCityViewController.h"
#import "CityListViewController.h"
@interface ChinaCityViewController ()<CityListViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *button;

@end

@implementation ChinaCityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)selectCityAction:(UIButton *)sender {
    CityListViewController *cityListView = [[CityListViewController alloc]init];
    cityListView.delegate = self;
    cityListView.arrayHotCity = [NSMutableArray arrayWithObjects:@"广州",@"北京",@"天津",@"厦门",@"重庆",@"福州",@"泉州",@"济南",@"深圳",@"长沙",@"无锡", nil];
    cityListView.arrayHistoricalCity = [NSMutableArray arrayWithObjects:@"福州",@"厦门",@"泉州", nil];
    cityListView.arrayLocatingCity   = [NSMutableArray arrayWithObjects:@"福州", nil];
    [self presentViewController:cityListView animated:YES completion:nil];
}

- (void)didClickedWithCityName:(NSString*)cityName
{
    [_button setTitle:cityName forState:UIControlStateNormal];
}

@end
