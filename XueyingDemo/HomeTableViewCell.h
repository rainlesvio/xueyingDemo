//
//  HomeTableViewCell.h
//  XueyingDemo
//
//  Created by wuxueying on 9/14/15.
//  Copyright (c) 2015 xueying wu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
