//
//  RapidSelectViewController.m
//  XueyingDemo
//
//  Created by wuxueying on 10/16/15.
//  Copyright © 2015 xueying wu. All rights reserved.
//

#import "RapidSelectViewController.h"
#import "PCRapidSelectionView.h"

@interface RapidSelectViewController ()
@property NSMutableArray *objects;
@end
@implementation RapidSelectViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
//    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UIButton *addButton = [[UIButton alloc] init];
    addButton.titleLabel.font = [UIFont systemFontOfSize:17.5];
    [addButton setTitle:@"Add Row" forState:UIControlStateNormal];
    [addButton addTarget:self action:@selector(insertNewObject:) forControlEvents:UIControlEventTouchUpInside];
    [addButton setTitleColor:self.view.tintColor forState:UIControlStateNormal];
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(secretMenu:)];
    longPress.minimumPressDuration = 0.3;
    [addButton addGestureRecognizer:longPress];
    [addButton sizeToFit];
    UIBarButtonItem *addButtonItem = [[UIBarButtonItem alloc] initWithCustomView:addButton];
    self.navigationItem.rightBarButtonItem = addButtonItem;
}

- (void)secretMenu:(UIGestureRecognizer *)gesture
{
    [PCRapidSelectionView viewForParentView:self.navigationController.view currentGuestureRecognizer:gesture interactive:YES options:@[@"Enable Debug",@"Inspect View",@"Change Gestures",@"More..."] title:@"Advanced Options" completionHandler:^(NSInteger selectedIndex)
     {
         NSLog(@"Secret menu selected: %i",(int)selectedIndex);
     }];
}

- (void)viewWillAppear:(BOOL)animated {
    self.clearsSelectionOnViewWillAppear = self.splitViewController.isCollapsed;
    [super viewWillAppear:animated];
}

- (void)insertNewObject:(id)sender {
    if (!self.objects) {
        self.objects = [[NSMutableArray alloc] init];
    }
    [self.objects insertObject:[NSDate date] atIndex:0];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Table View
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSDate *object = self.objects[indexPath.row];
    cell.textLabel.text = [object description];
    
    if (cell.contentView.gestureRecognizers.count == 1)
    {
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.font = [UIFont systemFontOfSize:24];
        cell.contentView.backgroundColor = [UIColor colorWithHue:0.07*self.objects.count saturation:1.0 brightness:1.0 alpha:0.9];
        
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(openContextMenu:)];
        longPress.minimumPressDuration = 0.3;
        longPress.cancelsTouchesInView = YES;
        [cell.contentView addGestureRecognizer:longPress];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pushDetail:)];
        tap.cancelsTouchesInView = YES;
        [cell.contentView addGestureRecognizer:tap];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)pushDetail:(UITapGestureRecognizer *)gesture
{
    
}

- (void)openContextMenu:(UILongPressGestureRecognizer *)gesture
{
    NSIndexPath *indexPath = [self indexPathForGesture:gesture];
    
    __weak typeof(self) weakSelf = self;
    [PCRapidSelectionView viewForParentView:self.navigationController.view currentGuestureRecognizer:gesture interactive:YES options:@[@"Show Detail",[NSString stringWithFormat:@"Delete Row %i",(int)indexPath.row + 1]] title:@"Quick Select" completionHandler:^(NSInteger selectedIndex)
     {
         if (selectedIndex == NSNotFound)
         {
             NSLog(@"Cancelled");
         }
         if (selectedIndex == 0)
         {
             
         }
         if (selectedIndex == 1)
         {
             [weakSelf tableView:weakSelf.tableView commitEditingStyle:UITableViewCellEditingStyleDelete forRowAtIndexPath:indexPath];
         }
     }];
}

- (NSIndexPath *)indexPathForGesture:(UIGestureRecognizer *)gesture {
    return [self.tableView indexPathForCell:(id)gesture.view.superview];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.objects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
    }
}

@end
