//
//  ViewController.m
//  FlatSlideControl
//
//  Created by Alexey Kubas on 9/21/15.
//  Copyright (c) 2015 Appus. All rights reserved.
//

#import "FlatSlideViewController.h"
#import "FlatSlideControl.h"

@interface FlatSlideViewController ()
@property (weak, nonatomic) IBOutlet FlatSlideControl *slideControl;
@property (weak, nonatomic) IBOutlet UILabel *valuesLabel;

@end

@implementation FlatSlideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.slideControl setLeftValue:0
                         rightValue:3];
    [self validateControlsValues];
}

-(void)updateLabels{
    self.valuesLabel.text = [NSString stringWithFormat:@"%lu - %lu", self.slideControl.leftValue, self.slideControl
                             .rightValue ];
}

-(void)validateControlsValues{
    [self updateLabels];
}

- (IBAction)actionBarValueChanged:(FlatSlideControl*)sender {
    [self validateControlsValues];
}

@end
