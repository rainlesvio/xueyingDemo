//
//  MinToMaxSliderViewController.m
//  XueyingDemo
//
//  Created by wuxueying on 12/8/15.
//  Copyright © 2015 xueying wu. All rights reserved.
//

#import "MinToMaxSliderViewController.h"
#import "ADo_MinToMaxSlider.h"

@interface MinToMaxSliderViewController ()

@end

@implementation MinToMaxSliderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    ADo_MinToMaxSlider *slider = [[ADo_MinToMaxSlider alloc] initWithMaxValue:100];
    slider.frame = CGRectMake(0, 100, self.view.frame.size.width, 100);
    [self.view addSubview:slider];
    slider.minToMax = ^(int minValue,int maxValue) {
    };
}

@end
