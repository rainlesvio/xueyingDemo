//
//  RevelImageViewController.m
//  XueyingDemo
//
//  Created by wuxueying on 10/23/15.
//  Copyright © 2015 xueying wu. All rights reserved.
//

#import "RevelImageViewController.h"
#import "UIView+RevealAnimation.h"

@interface RevelImageViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIView *revealView;
@end

@implementation RevelImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)startAnimation:(id)sender {
    [self.revealView dd_outLineAnimation];
    [self.imageView dd_revealAnimation];
    self.imageView.image = [UIImage imageNamed:@"testimage.jpg"];
}

@end
