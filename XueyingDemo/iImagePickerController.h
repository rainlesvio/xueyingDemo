//
//  iImagePickerController.h
//  iimagePickerContoller
//
//  Created by Rajesh on 9/11/15.
//  Copyright (c) 2015 Org. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImagePickerViewController.h"

@interface iImagePickerController : UIViewController

@property(nonatomic, weak) ImagePickerViewController *viewController;

@end
