//
//  BannerViewController.m
//  XueyingDemo
//
//  Created by wuxueying on 9/14/15.
//  Copyright (c) 2015 xueying wu. All rights reserved.
//

#import "BannerViewController.h"
#import "GGBannerView.h"

@interface BannerViewController () <GGBannerViewDelegate>
@property (weak, nonatomic) IBOutlet GGBannerView *bannerView;
@end
@implementation BannerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSArray *imageArray = @[@"http://7xk68o.com1.z0.glb.clouddn.com/1.jpg",
                            @"http://7xk68o.com1.z0.glb.clouddn.com/2.jpg",
                            @"http://7xk68o.com1.z0.glb.clouddn.com/3.jpg",
                            @"http://7xk68o.com1.z0.glb.clouddn.com/4.jpg",
                            @"http://7xk68o.com1.z0.glb.clouddn.com/5.jpg",
                            ];
    GGBannerView *bannerView2 = [[GGBannerView alloc]initWithFrame:CGRectMake(0, 100, 320, 100)];
    [self.view addSubview:bannerView2];
    bannerView2.delegate = self;
    [bannerView2 configBanner:imageArray];
}

-(void)bannerView:(GGBannerView *)bannerView didSelectAtIndex:(NSUInteger)index{
    if (bannerView == self.bannerView) {
        NSLog(@"选中-- bannerView1 - %@",@(index));
    }else{
        NSLog(@"选中-- bannerView2 - %@",@(index));
    }
}


@end
