//
//  LoadingHubViewController.m
//  XueyingDemo
//
//  Created by wuxueying on 10/16/15.
//  Copyright © 2015 xueying wu. All rights reserved.
//

#import "LoadingHubViewController.h"
#import "iKYLoadingHubView.h"

@interface LoadingHubViewController ()
@end
@implementation LoadingHubViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    iKYLoadingHubView *loadingHubView = [[iKYLoadingHubView alloc] initWithFrame:CGRectMake(85, 80, 150, 150)];
    loadingHubView.center = self.view.center;
    [self.view addSubview:loadingHubView];
    [loadingHubView showHub];
}

@end
