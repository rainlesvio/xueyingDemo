//
//  ViewController.h
//  iimagePickerContoller
//
//  Created by Rajesh on 9/11/15.
//  Copyright (c) 2015 Org. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagePickerViewController : UIViewController

- (void)previewImages:(NSMutableArray*)arrImages;
- (void)previewImagesFromTopController:(NSMutableArray*)arrImages;

@end

