//
//  ViewController.m
//  
//
//  Created by wordoor－z on 16/1/14.
//  Copyright © 2016年 wzx. All rights reserved.
//

#import "WZXPopViewController.h"
#import "RootViewController.h"

@interface WZXPopViewController ()
@property(nonatomic,assign) BOOL isShow;
@end
@implementation WZXPopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _isShow = NO;
}

- (void)createPopVCWithRootVC:(UIViewController *)rootVC andPopView:(UIView *)popView{
    _rootVC = rootVC;
    _popView = popView;
    [self createUI];
}

- (void)createUI{
    self.view.backgroundColor = [UIColor blackColor];
    _rootVC.view.frame = self.view.bounds;
    _rootVC.view.backgroundColor = [UIColor whiteColor];
    _rootview = _rootVC.view;
    [self addChildViewController:_rootVC];
    [self.view addSubview:_rootview];
    
    _maskView = ({
        UIView * maskView = [[UIView alloc]initWithFrame:self.view.bounds];
        maskView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        maskView.alpha = 0;
        maskView;
    });
     [_rootview addSubview:_maskView];
}

- (void)close{
    _isShow = NO;
    CGRect frame = _popView.frame;
    frame.origin.y += self.view.frame.size.height/2.0;
    
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [_maskView setAlpha:0.f];
        _popView.frame = frame;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [_rootview.layer setTransform:CATransform3DIdentity];
        } completion:^(BOOL finished) {
             [_popView removeFromSuperview];
        }];
    }];
    
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [_rootview.layer setTransform:[self firstTransform]];
    } completion:^(BOOL finished) {
    }];
}

- (void)show{
    [[UIApplication sharedApplication].windows[0] addSubview:_popView];
    CGRect frame = _popView.frame;
    frame.origin.y = self.view.frame.size.height/2.0;
    
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [_rootview.layer setTransform:[self firstTransform]];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [_rootview.layer setTransform:[self secondTransform]];
            [_maskView setAlpha:0.5f];
            _popView.frame = frame;
        } completion:^(BOOL finished) {
        }];
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (!_isShow){
        [self show];
    }
    _isShow = YES;
}

- (CATransform3D)firstTransform{
    CATransform3D t1 = CATransform3DIdentity;
    t1.m34 = 1.0/-900;
    t1 = CATransform3DScale(t1, 0.95, 0.95, 1);
    t1 = CATransform3DRotate(t1, 15.0 * M_PI/180.0, 1, 0, 0);
    return t1;
}

- (CATransform3D)secondTransform{
    CATransform3D t2 = CATransform3DIdentity;
    t2.m34 = [self firstTransform].m34;
    t2 = CATransform3DTranslate(t2, 0, self.view.frame.size.height * (-0.08), 0);
    t2 = CATransform3DScale(t2, 0.8, 0.8, 1);
    return t2;
}

@end
