//
//  VisualEffectViewController.m
//  XueyingDemo
//
//  Created by wuxueying on 9/15/15.
//  Copyright (c) 2015 xueying wu. All rights reserved.
//

#import "VisualEffectViewController.h"

@interface VisualEffectViewController ()
@end
@implementation VisualEffectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"毛玻璃";
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    [imageView setImage:[UIImage imageNamed:@"1.jpg"]];
    
    UIVisualEffectView *visualEfView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleLight]];
    visualEfView.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height/2);
    visualEfView.alpha = 0.9;
    [imageView addSubview:visualEfView];
    
    [self.view addSubview:imageView];
}

@end
