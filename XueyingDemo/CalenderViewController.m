//
//  CalenderViewController.m
//  XueyingDemo
//
//  Created by wuxueying on 9/15/15.
//  Copyright (c) 2015 xueying wu. All rights reserved.
//

#import "CalenderViewController.h"
#import "SZCalendarPicker.h"

@interface CalenderViewController ()
@end
@implementation CalenderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"日历选择";
}

- (IBAction)show:(id)sender {
    SZCalendarPicker *calendarPicker = [SZCalendarPicker showOnView:self.view];
    calendarPicker.today = [NSDate date];
    calendarPicker.date = calendarPicker.today;
    calendarPicker.frame = CGRectMake(0, 100, self.view.frame.size.width, 300);
    calendarPicker.calendarBlock = ^(NSInteger day, NSInteger month, NSInteger year){
        NSLog(@"%i-%i-%i", year,month,day);
    };
}

@end
