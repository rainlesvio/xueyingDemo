//
//  InputDodgerViewController.m
//  XueyingDemo
//
//  Created by wuxueying on 10/16/15.
//  Copyright © 2015 xueying wu. All rights reserved.
//

#import "InputDodgerViewController.h"
#import "UIView+MLInputDodger.h"

@interface InputDodgerViewController ()
@end
@implementation InputDodgerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.view.shiftHeightAsDodgeViewForMLInputDodger = 10;
    [self.view registerAsDodgeViewForMLInputDodger];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

@end
