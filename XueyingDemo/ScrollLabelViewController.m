//
//  ScrollLabelViewController.m
//  XueyingDemo
//
//  Created by wuxueying on 9/22/15.
//  Copyright © 2015 xueying wu. All rights reserved.
//

#import "ScrollLabelViewController.h"
#import "IoGMarquee.h"

@interface ScrollLabelViewController ()
@property (weak, nonatomic) IBOutlet IoGMarquee *liveFeedDisplay;
@end
@implementation ScrollLabelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString			*marqueeMessage = @"      This is a test of the Emergency Broadcast System. The broadcasters of your area in voluntary cooperation with the FCC and federal, state and local authorities have developed this system to keep you informed in the event of an emergency.";
    
    [self.liveFeedDisplay setupForMessage:marqueeMessage withBackgroundColor:[UIColor blueColor] andForegroundColor:[UIColor greenColor]];
}

@end
