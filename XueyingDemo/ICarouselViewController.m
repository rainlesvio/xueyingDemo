//
//  ICarouselViewController.m
//  XueyingDemo
//
//  Created by wuxueying on 9/17/15.
//  Copyright (c) 2015 xueying wu. All rights reserved.
//

#import "ICarouselViewController.h"
#import "iCarousel.h"
#define SCREENHEIGHT [UIScreen mainScreen].bounds.size.height
#define SCREENWIDTH [UIScreen mainScreen].bounds.size.width

@interface ICarouselViewController () <iCarouselDataSource, iCarouselDelegate> {
    NSArray *_arname;
}
@end
@implementation ICarouselViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"iCarousel";
    _arname = [NSArray arrayWithObjects:@"125.jpg",@"125.jpg",@"125.jpg",@"125.jpg",@"125.jpg",@"125.jpg", nil];
    iCarousel * ic = [[iCarousel alloc]initWithFrame:CGRectMake(110, 64, 100, 100)];
    ic.delegate =self;
    ic.dataSource =self;
    ic.type = iCarouselTypeRotary;
    [self.view addSubview:ic];
}

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel {
    return _arname.count;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view {
    UIImageView  *img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 120.0f, 120.0f)];
    img.image = [UIImage imageNamed:_arname[index]];
    view.contentMode = UIViewContentModeCenter;
    return img;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value {
    if (option == iCarouselOptionSpacing)
    {
        return value * 1.2;
    }
    return value;
}

@end
