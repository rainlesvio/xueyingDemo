//
//  BEMMainViewController.m
//  CheckBox
//
//  Created by Bobo on 9/21/15.
//  Copyright (c) 2015 Boris Emorine. All rights reserved.
//

#import "BEMMainViewController.h"
#import "BEMCheckBox.h"

@interface BEMMainViewController ()
@property (strong, nonatomic) IBOutlet BEMCheckBox *checkBox;
@end
@implementation BEMMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.checkBox.onAnimationType = BEMAnimationTypeBounce;
    self.checkBox.offAnimationType = BEMAnimationTypeBounce;
}

@end
