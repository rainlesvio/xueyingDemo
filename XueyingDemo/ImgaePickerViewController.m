//
//  ViewController.m
//  BDImagePicker
//
//  Created by Suteki on 16/1/20.
//  Copyright © 2016年 Baidu. All rights reserved.
//

#import "ImgaePickerViewController.h"
#import "BDImagePicker.h"

@interface ImgaePickerViewController ()

@end

@implementation ImgaePickerViewController

- (IBAction)toggleAvatar:(UIButton *)sender {
    [BDImagePicker showImagePickerFromViewController:self
                                       allowsEditing:YES
                                        finishAction:^(UIImage *image) {
        if (image) {
            [sender setBackgroundImage:image forState:UIControlStateNormal];
        }
    }];
}

@end
