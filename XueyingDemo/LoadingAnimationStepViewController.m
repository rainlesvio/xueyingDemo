//
//  LoadingAnimationStepViewController.m
//  XueyingDemo
//
//  Created by wuxueying on 12/9/15.
//  Copyright © 2015 xueying wu. All rights reserved.
//

#import "LoadingAnimationStepViewController.h"
#import "OneLoadingAnimationView.h"

@interface LoadingAnimationStepViewController ()
@property (weak, nonatomic) IBOutlet OneLoadingAnimationView *loadingView;
@end
@implementation LoadingAnimationStepViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}
- (IBAction)tap:(id)sender {
    [self.loadingView startAnimation];
}

@end
