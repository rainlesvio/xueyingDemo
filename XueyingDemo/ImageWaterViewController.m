
/**
 *  ViewController.m
 */
#import "ImageWaterViewController.h"
#import "UIImage+LL.h"

@interface ImageWaterViewController ()<UIAlertViewDelegate>

@property(nonatomic, weak) UIImageView *imageView;

@end

@implementation ImageWaterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 80, 375, 587)];
    self.imageView = imageView;
    imageView.image = [UIImage imageNamed:@"bg"];
    NSString *path = [[NSBundle mainBundle]pathForResource:@"bg" ofType:@".jpeg"];
    self.imageView.image = [UIImage imageWithContentsOfFile:path];
    [self.view addSubview:imageView];
}

- (IBAction)makeWaterMark:(UIButton *)sender {
    UIImage *image = [UIImage waterMarkWithImageName:@"bg.jpeg" andMarkImageName:@"logo"];
    _imageView.image = image;
}

@end
