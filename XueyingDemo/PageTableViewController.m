//
//  PageTableViewController.m
//  XueyingDemo
//
//  Created by wuxueying on 9/17/15.
//  Copyright (c) 2015 xueying wu. All rights reserved.
//

#import "PageTableViewController.h"

#define kWindowHeight 100.0f
#define kImageHeight 200.0f

@interface PageTableViewController ()<UITableViewDataSource, UITableViewDelegate>{
    CGFloat _animateFactor;
}
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIScrollView *scrollView;
@end
@implementation PageTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"pull down to show photo";
    self.scrollView.frame = CGRectMake(0,kWindowHeight - kImageHeight+(kImageHeight-kWindowHeight)/2.0f, self.view.frame.size.width, kImageHeight - kWindowHeight + self.view.frame.size.height);
    self.tableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (!section) {
        return 1;
    }
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellReuseIdentifier   = @"cell";
    static NSString *windowReuseIdentifier = @"clean";
    UITableViewCell *cell = nil;
    if (indexPath.section == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:windowReuseIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:windowReuseIdentifier];
            cell.backgroundColor = [UIColor clearColor];
            cell.contentView.backgroundColor = [UIColor clearColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
    }
    else
    {
        cell = [tableView dequeueReusableCellWithIdentifier:cellReuseIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellReuseIdentifier];
            cell.selectionStyle  = UITableViewCellSelectionStyleNone;
        }
        cell.textLabel.text = [NSString stringWithFormat:@"test %d",indexPath.row];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0){
        return kWindowHeight;
    }
    return 44;
}

- (void) scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self updateOffsets];
}

- (void)updateOffsets {
    CGFloat yOffset   = _tableView.contentOffset.y;
    CGFloat threshold = kImageHeight - kWindowHeight;
    
    if (yOffset > -threshold && yOffset < 0) {
        
        _scrollView.contentOffset = CGPointMake(0.0, floorf(yOffset / 2.0));
        
    } else if (yOffset < 0) {
        
        _scrollView.contentOffset = CGPointMake(0.0, yOffset + floorf(threshold / 2.0));
        
    } else {
        
        _scrollView.contentOffset = CGPointMake(0.0, yOffset);
        
    }
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] init];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

- (UIScrollView *)scrollView
{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_scrollView];
        UIImageView *imageView = [[UIImageView alloc] init];
        imageView.backgroundColor = [UIColor clearColor];
        imageView.frame = CGRectMake(0, 0, self.view.frame.size.width, kImageHeight);
        imageView.clipsToBounds = YES;
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.image = [UIImage imageNamed:@"demo"];
        [_scrollView addSubview:imageView];
    }
    return _scrollView;
}

@end
