//
//  SpinnerViewController.m
//  XueyingDemo
//
//  Created by wuxueying on 9/15/15.
//  Copyright (c) 2015 xueying wu. All rights reserved.
//

#import "SpinnerViewController.h"

@interface SpinnerViewController ()
@end
@implementation SpinnerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"加载动画";
    JTMaterialSpinner *spinnerView = [JTMaterialSpinner new];
    spinnerView.frame = CGRectMake(100, 100, 40, 40);
    spinnerView.center = self.view.center;
    [self.view addSubview:spinnerView];
    
    spinnerView.circleLayer.lineWidth = 2.0;
    spinnerView.circleLayer.strokeColor = [UIColor orangeColor].CGColor;
    [spinnerView beginRefreshing];
}

@end
